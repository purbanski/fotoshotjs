var GRABABLE_MASK_BIT = 1<<31;
var NOT_GRABABLE_MASK = ~GRABABLE_MASK_BIT;

//World.prototype.timeDelta = 0;
//World.prototype.shotIndex = 0;

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
World = function() {
	var space = this.space = new cp.Space();
	this.remainder = 0;
	this.fps = 0;
	this.mouse = v(0,0);
	this.simulationTime = 0;
	this.drawTime = 0;

	
	this.timeDelta = 0;
	this.shotIndex = 0;
	
//	this.canvas = canvas;
//	this.ctx = ctx;
	
	var self = this;
	var canvas2point = this.canvas2point = function(x, y) {
		return v(x / self.scale, 480 - y / self.scale);
	};

	this.point2canvas = function(point) {
			return v(point.x * self.scale, (480 - point.y) * self.scale);
	};
};
var canvas 	= World.prototype.cavas = document.getElementById('can');
var ctx 	= World.prototype.ctx =   canvas.getContext('2d');

//-------------------------------------------------------------------------------------
// The physics space size is 640x480, with the origin in the bottom left.
// Its really an arbitrary number except for the ratio - everything is done
// in floating point maths anyway.
window.onresize = function(e) 
{
	var width = World.prototype.width = gCanvas.width = window.innerWidth;
	var height = World.prototype.height = gCanvas.height = window.innerHeight;

	if (iOS || Android)
		gWindowScale = gMobileScale;
	else
	{
		gWindowScale = height / gFixedHeight ;

//		if (width/height > gFixedWidth/gFixedHeight) 
//			gWindowScale = height / gFixedHeight ;
//		else 
//			gWindowScale = width / gFixedWidth ;
//		
//		World.prototype.scale = 1.5;
	}

	World.prototype.scale = gWindowScale;
	World.resized = true;
};
window.onresize();
//-------------------------------------------------------------------------------------
var raf = window.requestAnimationFrame
	|| window.webkitRequestAnimationFrame
	|| window.mozRequestAnimationFrame
	|| window.oRequestAnimationFrame
	|| window.msRequestAnimationFrame
	|| function(callback) {
		return window.setTimeout(callback, 1000 / 60);
	};
//-------------------------------------------------------------------------------------
// These should be overridden by the demo itself.
World.prototype.update = function(dt) {
	this.space.step(dt);
};
//-------------------------------------------------------------------------------------
World.prototype.run = function() 
{
	this.running = true;

	var self = this;
	var lastTime = 0;
	var step = function(time) {
		self.step(time - lastTime);
		lastTime = time;

		if (self.running) {
			raf(step);
		}
		else 
		{
			app = new FotoShot;
			app.run();
		}
	};

	step(0);
};
//-------------------------------------------------------------------------------------
World.prototype.stop = function() {
//	console.log("stopping...");
	this.running = false;
};
//-------------------------------------------------------------------------------------
World.prototype.processShot = function( shot )
{ 
	var xd, yd;
	var xmax;
	var xs, ys;
	
	xs = gImgScale * parseFloat(shot['xs']) ;
	ys = gImgScale * parseFloat(shot['ys']) ;

	xmax = gTrackWidth * gImgScale;
	ymax = gTrackHeight * gImgScale;
	
	xd = (gFixedWidth-xmax) / 2;
	yd = (gFixedHeight - ymax ) / 2;
	bullet = new puBullet(this, gBulletRadius * gImgScale * gBubbleScale, xs + xd + gCenterMoveX, yd + ys + gCenterMoveY );
	bullet.body.setVel(cp.v( parseFloat(shot['xv']) * gScaleVelocity, parseFloat(shot['yv'])  * gScaleVelocity ));
	bullets.push( bullet );
}
//-------------------------------------------------------------------------------------
World.prototype.trackerUpdate = function(dt) 
{
//	console.log("time delta "+dt );
	// on the restart I'm getting single incremental value like 11.3 
	// 13.5, 22, 33 and so on, this hack will fix it
	if (dt > 1 )
		return;
	
	if ( typeof(fsJsonShotTrack[0]['shots'])=='undefined')
	{
//		console.log("problem");
		return;
	}
	
	var shots = fsJsonShotTrack[0]['shots'];
	if ( this.shotIndex >= shots.length && this.shotIndex != -1 )
	{
		this.shotIndex = -1;

		var trackDone = false;
		var imgDone = false;
		
		var newTrack = fsJsonShotTrack;
		var newImg = fsJsonImg;

		if (iOS || Android)
		{
			setTimeout(function () {

				var updateCheck = function()
				{
					if ( imgDone && trackDone )
					{
						fsJsonImg = newImg;
						fsJsonShotTrack = newTrack;
						
						GlobalVarUpdate();
					}
				}
				
				if ( ! gUserImgSet )
					$.get("php/api/getRandomImg.php",function(data,status){
						if (status=='success' )
						{
							newImg = jQuery.parseJSON(data);
						}
						imgDone = true;
						updateCheck();
					  });
				else
				{
					newImg = fsJsonImg;
					imgDone = true;
				}
				
				if ( ! gUserTrackSet )
					$.get("php/api/getRandomTrack.php",function(data,status){
						if (status=='success' )
						{
						    newTrack = jQuery.parseJSON(data);
						}
						trackDone = true;
						updateCheck();
					  });
				else
				{
					newTrack = fsJsonShotTrack;
					trackDone = true;
				}
			
				updateCheck();
			}, 1000 );
		}
		else
		{
			setTimeout(function () {
				
				if ( ! gReplayEnable )
					return;
				
				var restartCheck = function()
				{
					if ( imgDone && trackDone && gReplayEnable)
					{
						fsJsonImg = newImg;
						fsJsonShotTrack = newTrack;
						
						GlobalVarUpdate();
						restartApp();
					}
					
				}
				
				if ( ! gUserImgSet )
					$.get("php/api/getRandomImg.php",function(data,status) {
						if (status=='success' )
						{
							newImg = jQuery.parseJSON(data);
						}
						imgDone = true;
						restartCheck();
					  });
				else 
				{
					imgDone = true;
					newImg = fsJsonImg;
				}
				
				if ( ! gUserTrackSet )
				{
					$.get("php/api/getRandomTrack.php",function(data,status){
						if (status=='success' )
						{
						     newTrack = jQuery.parseJSON(data);
						}
						trackDone = true;
						restartCheck();
					  });
				}
				else 
				{
					trackDone = true;
					newTrack = fsJsonShotTrack;
				}
				
				restartCheck();
				
			}, gDemoRestartAfterSec * 1000 );
		}
		return;
	}

	if ( this.shotIndex == -1 )
		return;

	this.timeDelta += dt;
	while (this.shotIndex < shots.length && this.timeDelta>=shots[this.shotIndex]['time'] )
	{
		this.processShot( shots[this.shotIndex] );
		this.shotIndex++;
	}
}
//-------------------------------------------------------------------------------------
World.prototype.step = function(dt) {
	
//	if ( ! gAppLoaded )
//		return;
	
	if (gReplayEnable) this.trackerUpdate(dt/1000);
	
	// Update FPS
	if(dt > 0) {
		this.fps = 0.9*this.fps + 0.1*(1000/dt);
	}

	var lastNumActiveShapes = this.space.activeShapes.count;
	var now = Date.now();
	this.update(1/60);
	this.simulationTime += Date.now() - now;

	// Only redraw if the simulation isn't asleep.
	if (lastNumActiveShapes > 0 || World.resized) {
		now = Date.now();
		this.draw();
		this.drawTime += Date.now() - now;
		World.resized = false;
	}
};
//-------------------------------------------------------------------------------------
World.prototype.drawInfo = function() {
	var space = this.space;

	var maxWidth = this.width - 20;

	this.ctx.textAlign = 'start';
	this.ctx.textBaseline = 'alphabetic';
	this.ctx.fillStyle = "white";
	//this.ctx.fillText(this.ctx.font, 100, 100);
	var fpsStr = Math.floor(this.fps * 10) / 10;
	if (space.activeShapes.count === 0) {
		fpsStr = '--';
	}
	this.ctx.fillText("FPS: " + fpsStr, 10, 50, maxWidth);
	this.ctx.fillText("Step: " + space.stamp, 10, 80, maxWidth);

	var arbiters = space.arbiters.length;
	this.maxArbiters = this.maxArbiters ? Math.max(this.maxArbiters, arbiters) : arbiters;
	this.ctx.fillText("Arbiters: " + arbiters + " (Max: " + this.maxArbiters + ")", 10, 110, maxWidth);

	var contacts = 0;
	for(var i = 0; i < arbiters; i++) {
		contacts += space.arbiters[i].contacts.length;
	}
	this.maxContacts = this.maxContacts ? Math.max(this.maxContacts, contacts) : contacts;
	this.ctx.fillText("Contact points: " + contacts + " (Max: " + this.maxContacts + ")", 10, 140, maxWidth);
	this.ctx.fillText("Simulation time: " + this.simulationTime + " ms", 10, 170, maxWidth);
	this.ctx.fillText("Draw time: " + this.drawTime + " ms", 10, 200, maxWidth);

	if (this.message) {
		this.ctx.fillText(this.message, 10, this.height - 50, maxWidth);
	}
};
//-------------------------------------------------------------------------------------
World.prototype.draw = function() 
{
};
