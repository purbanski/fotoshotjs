gCanvas = document.getElementById('can');
gCtx 	= gCanvas.getContext('2d');
//------------------------------------------------------------------------
gCenterMoveX = -30;
gCenterMoveY = -10;

gReplayEnable = true;

gUserImgSet 	= false;
gUserTrackSet	= false;

gFixedWidth = 640;
gFixedHeight = 480;

gMobileScale = 2.2;

gBallRadius 	= 5;
gBulletRadius 	= 8;
gScaleVelocity 	= 0.21;

gDemoRestartAfterSec = 10.5;
gAppLoaded 	= false;

iOS 	= false;
Android = false;
//------------------------------------------------------------------------
var GlobalVarUpdate = function()
{
	gBubbleScale = parseFloat(fsJsonImg[0].bubbleScale);;
	gImgWidth 	=  parseInt(fsJsonImg[0].width);
	gImgHeight 	=  parseInt(fsJsonImg[0].height);

	gTrackWidth 	=  parseInt(fsJsonShotTrack[0].width);
	gTrackHeight 	=  parseInt(fsJsonShotTrack[0].height);

	// gImgScale = 480 / gTrackHeight;
	gImgScale= 0.6/gBubbleScale*(50/gImgHeight);

	if (iOS || Android)
	{
		gCenterMoveX = -100;
		gCenterMoveY = -25;
		
		gImgScale= 0.64/gBubbleScale*(50/gImgHeight);
	}
}
//------------------------------------------------------------------------
var AppInit = function()
{
	///-----------------
	// setup
	///-------------
	var i = 0, iDevice = ['iPad', 'iPhone', 'iPod'];
	for ( ; i < iDevice.length ; i++ ) 
		if( navigator.platform === iDevice[i] ){ iOS = true; break; }
	
	//-------------
	var ua = navigator.userAgent.toLowerCase();
	Android = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
}
//------------------------------------------------------------------------
AppInit();
GlobalVarUpdate();
//------------------------------------------------------------------------
gTemp = 0;