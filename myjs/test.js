 
var image_width = 38;
var image_height = 35;
var image_row_length = 24;

var bodyCount = 0;
var myImage = [{"r":255, g:'0', b:'255'}];

var balls = [];
var bullets = [];

var sharedFotoShot = {};


var FotoShot = function()
{
	World.call(this);

	var space = this.space;
	space.setIterations(6);
	// The space will contain a very large number of similary sized objects.
	// This is the perfect candidate for using the spatial hash.
	// Generally you will never need to do this.
	//
	// (... Except the spatial hash isn't implemented in JS)
	//cpSpaceUseSpatialHash(space, 2.0, 10000);
	
	bodyCount = 0;
	
	var radius;

	var xstart, ystart;
	var xd, yd;
	
	radius = 3;
	xd = 2 * radius;
	yd = 2 * radius;
	
	xstart = 320;
	ystart = 440;
	
	for ( var yi = 0; yi < fsJsonImg[0].height; yi++)
	{
		for ( var xi = 0; xi < fsJsonImg[0].width; xi++)
		{
			var color;
			var index;
			index = xi + yi * fsJsonImg[0].width;
			color = fsJsonImg[0].balls[index];
			
			
			var ball = new puBall(this,
					radius,
					xstart + xd*xi,
					ystart - yd*yi
					);
			balls.push( ball );
			ball.setColor(color[0], color[1], color[2]);
		}
	}

	var bullet;
	
//	bullet = new puBullet(this,	5, 550, 330 );
//	bullet.body.setVel(cp.v(-400, -300));
//	bullets.push( bullet );
	
//	bullet = new puBullet(this,	5, 150, 330 );
//	bullet.body.setVel(cp.v(400, -360));
//	bullets.push( bullet );
//
//	var shape = space.addShape(new cp.CircleShape(body, 8, cp.vzero));
	
	this.shooter = new puShooter( this );
	sharedFotoShot  = this;
};

FotoShot.prototype = Object.create(World.prototype);

FotoShot.prototype.restart = function()
{
}

FotoShot.prototype.draw = function()
{
	var ctx = this.ctx;

	var self = this;

	// Draw shapes
	ctx.strokeStyle = 'black';
	ctx.clearRect(0, 0, this.width, this.height);

	this.ctx.font = "16px sans-serif";
	this.ctx.lineCap = 'round';

	for ( var i = 0; i < balls.length; i++ )
	{
		var ball;
		ball = balls[i];
		ball.render();
	}

	for ( var i = 0; i < bullets.length; i++ )
	{
		var bullet;
		bullet = bullets[i];
		bullet.render();
	}

	this.shooter.render();
	
	//	this.space.eachShape(function(shape) {
//		if (shape.ball) {
//			ctx.fillStyle = shape.style();
//			shape.draw(ctx, self.scale, self.point2canvas);
//		} else {
////			drawRect(ctx, self.point2canvas, cp.v.sub(shape.tc, cp.v(1, 1)), cp.v(2, 2));
//		}
//	});

	this.drawInfo();
};

addDemo('Logo Smash2', FotoShot);

