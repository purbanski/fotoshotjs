var balls = [];
var bullets = [];

var debsize = 0;
//-------------------------------------------------------------------------------------

var FotoShot = function()
{
	this.stopped = false;
	
	var rand = function(min,max)
	{
		if (min>max) {
			var t = min;
			min = max;
			max = t;
		}
		
		var range = max - min;
		var ret;
		
		ret = Math.random() * range + min;
		
		return parseInt(ret);
	}

	var rc = function(){ return rand(0,255); }
	this.randomColor = "rgb("+rc()+","+rc()+","+rc()+")";
	

	debsize +=1;
	World.call(this);
	
//	ShotPlayer.call(this);
	
	this.shooter = new puShooter();
//	sharedFotoShot  = this;
//
	var space = this.space;
	space.setIterations(6);
	// The space will contain a very large number of similary sized objects.
	// This is the perfect candidate for using the spatial hash.
	// Generally you will never need to do this.
	//
	// (... Except the spatial hash isn't implemented in JS)
	//cpSpaceUseSpatialHash(space, 2.0, 10000);
	
	var xd, yd;
	var xs, ys;
	var xsp, ysp;
	
	xd = 2 * gBallRadius * gImgScale * gBubbleScale;
	yd = 2 * gBallRadius * gImgScale * gBubbleScale;
	
	xsp = gFixedWidth / 2;
	ysp = gFixedHeight / 2;
	
	xs = xsp - ( gImgWidth / 2) * (gBallRadius * 2) * gImgScale * gBubbleScale;
	ys = ysp + ( gImgHeight / 2) * (gBallRadius * 2) * gImgScale * gBubbleScale;
	
	for ( var yi = 0; yi < gImgHeight; yi++)
	{
		for ( var xi = 0; xi < gImgWidth; xi++)
		{
			var color;
			var index;
			index = xi + yi * gImgWidth;
			
			color = fsJsonImg[0].balls[index];
			
			// should not happen - but happens :)
			if (typeof color == 'undefined')
			{
				break;
			}
			
			var ball = new puBall(this,
					gBallRadius * gImgScale * gBubbleScale,
					xs + xd*xi + gCenterMoveX,
					ys - yd*yi + gCenterMoveY
					);
			balls.push( ball );
			ball.setColor(color[0], color[1], color[2]);
		}
	}
	

//	var cb = function( self, x, y )
//	{
//		var ball;
//		ball = new puBall(self,	gBallRadius, x, y );
//		balls.push( ball );
//		ball.setColor(255, 0,0);
//	}
//	var y1 = 0;
//	cb( this, 0,y1);
//	cb( this, 320,y1);
//	cb( this, 640,y1);
//
//	var y2=11380;
//	cb( this, 0,y2);
//	cb( this, 320,y2);
//	cb( this, 640,y2);
//
//	var y3=470;
//	cb( this, 0,y3);
//	cb( this, 320,y3);
//	cb( this, 640,y3);

	
};
//-------------------------------------------------------------------------------------

var parent = Object.create(World.prototype);

FotoShot.prototype = parent;
//-------------------------------------------------------------------------------------
FotoShot.prototype.restart = function()
{
}
//-------------------------------------------------------------------------------------
FotoShot.prototype.myStop = function()
{
	bullets=[];
	balls=[];
	
	if (this.shooter) this.shooter.myStop();
	this.shooter = null;
//	console.log("stop 1");
	stop();
	stopped = true;
	
//	parent.stop();
}
//-------------------------------------------------------------------------------------
FotoShot.prototype.draw = function()
{
	if (this.stopped)
		return;
	
	var ctx = this.ctx;
	var self = this;

	ctx.fillStyle = this.randomColor;
//	ctx.strokeStyle='red';
//	ctx.fillStyle = 'rgb(0,255,255)';
	ctx.clearRect(0, 0, this.width, this.height);

//	drawCircle(gCtx, 
//			this.scale, 
//			this.point2canvas, 
//			cp.v(10,20), 
//			200 - debsize );

	this.ctx.font = "16px sans-serif";
	this.ctx.lineCap = 'round';

	for ( var i = 0; i < balls.length; i++ )
	{
		var ball;
		ball = balls[i];
		ball.render();
	}

	for ( var i = 0; i < bullets.length; i++ )
	{
		var bullet;
		bullet = bullets[i];
		bullet.render();
	}

	if (this.shooter)
		this.shooter.render();
	//	this.drawInfo();
};

