var startPoint = { x: 0, y:0 };
var stopPoint =  { x: 0, y:0 };

//---------------------------------------------------		
var getMouseX = function( e, ins, shooter )
{
	var x;
	x = e.clientX / app.scale;
	return x;
}
//---------------------------------------------------
var getMouseY = function( e,ins, shooter )
{
	var y;
	y = ( window.innerHeight - e.clientY ) / app.scale;
	return y;
}
//---------------------------------------------------
var puShooter = function()
{
	var c=1;
	var self = this;
	this.stopped = false;
	
	this.StatsEnum = {
		idle : 0,
		clicked : 1,
		released : 2
	}
		   
	this.state = this.StatsEnum.idle;
	
	this.canvas = canvas = $("#can");
	
	
//	canvas.bind( "touchstart", function(e){
//		_this.state = _this.StatsEnum.clicked;
//		
//		startPoint.x = getMouseX(e, $(this), _this);
//		startPoint.y = getMouseY(e, $(this), _this);
//
//		stopPoint.x = startPoint.x;
//		stopPoint.y = startPoint.y;
//	});
	
//	this.startPoint = v(0,0);
	canvas.mousedown(function(e) {
		self.state = self.StatsEnum.clicked;
		
		startPoint.x = getMouseX(e, $(this), self);
		startPoint.y = getMouseY(e, $(this), self);

		stopPoint.x = startPoint.x;
		stopPoint.y = startPoint.y;
	});

	
//	canvas.bind( "touchend", function(e){
//		console.log( "mouse up." );
//		_this.state = _this.StatsEnum.idle;
//		
//		var bullet;
//		var x;
//		var y;
//		var ratio;
//		
//		ratio = 5;
//		
//		x = ratio * ( startPoint.x - stopPoint.x );
//		y = ratio * ( startPoint.y - stopPoint.y );
//		
//		console.log(startPoint.x + " " + startPoint.y);
//		bullet = new puBullet(_this.world, 5, stopPoint.x, stopPoint.y );
//		bullet.body.setVel(cp.v(x, y));
//		bullets.push( bullet );
//
//		_this.resetPoints();
//	});
	
	var shoot = function()
	{
		trackShot();
		
		var bullet;
		var x;
		var y;
		var ratio;
		
		ratio = 5;
		
		x = ratio * ( startPoint.x - stopPoint.x );
		y = ratio * ( startPoint.y - stopPoint.y );
		
		bullet = new puBullet(app, gBulletRadius * gImgScale * gBubbleScale , stopPoint.x, stopPoint.y );
		bullet.body.setVel(cp.v(x, y));
		bullets.push( bullet );
	}
	
	canvas.mouseup(function() {
		self.state = self.StatsEnum.idle;
		shoot();
		self.resetPoints();

	});

	canvas.mousemove(function(e) {
		if ( self.state != self.StatsEnum.clicked )
			return;
		
		stopPoint.x = getMouseX(e, $(this), self);
		stopPoint.y = getMouseY(e, $(this), self);
	});

}
//-------------------------------------------------------------------------------------
puShooter.prototype.myStop = function()
{
	this.canvas.unbind("mousedown");
	this.canvas.unbind("mousemove");
	this.canvas.unbind("mouseup");
	
	this.stopped = true;
}
//-------------------------------------------------------------------------------------
puShooter.prototype.resetPoints = function()
{
	startPoint.x = stopPoint.x = 0;
	startPoint.y = stopPoint.y = 0;
}
//-------------------------------------------------------------------------------------
puShooter.prototype.render = function()
{
	if ( this.stopped )
		return;
	
	gCtx.fillStyle = 'rgb(0,255,0)';
	gCtx.strokeStyle = 'rgb(0,255,0)';
	
	gCtx.lineWidth = 16 * gImgScale * gBubbleScale;
	drawLine( gCtx, app.point2canvas, startPoint, stopPoint );
	
	var point = {x:0, y:0};
	
	point.x = startPoint.x - ( stopPoint.x - startPoint.x );
	point.y = startPoint.y - ( stopPoint.y - startPoint.y );
	
	gCtx.lineWidth = 3 * gImgScale * gBubbleScale;	
	drawLine( gCtx, app.point2canvas, startPoint, point );

//	point.x = startPoint.x - ( stopPoint.x - startPoint.x ) / 2;
//	point.y = startPoint.y - ( stopPoint.y - startPoint.y ) / 2;
//
//	
//	gCtx.lineWidth = 10 * gImgScale * gBubbleScale;	
//	drawLine( gCtx, app.point2canvas, startPoint, point );

}
//-------------------------------------------------------------------------------------

