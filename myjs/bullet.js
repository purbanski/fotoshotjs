var layer = 0;

var puBullet = function(world,radius,x,y)
{
	this.radius = radius;
	this.body = new cp.Body(1000, Infinity);
	
	this.body.setPos(cp.v(x, y));
	
	this.shape = new cp.CircleShape(this.body, radius, cp.vzero);
	this.shape.setElasticity(0);
	this.shape.setFriction(0);
	
//	this.shape.setLayers(NOT_GRABABLE_MASK);
	this.shape.setLayers(Math.pow(2,layer));

	if ( layer == 31 ) // int overflow as 2^32 = maxInt 
		layer = 0;
	else
		layer++;
	
	this.shape.setCollisionType(0);
	
	this.world = world;
	
	this.color = 'rgb(0,255,0)';
	
	this.world.space.addBody(this.shape.getBody());
	this.world.space.addShape(this.shape);
	
	this.world.space.addCollisionHandler( 1, 0, null, null, null, null);
}

puBullet.prototype.setColor = function(r,g,b)
{
	var color;
	color = 'rgb('+ r + ',' + g + ',' + b +')';
	this.color = color;
}

puBullet.prototype.render = function()
{
//	ball.shape.draw(gCtx, self.scale, self.point2canvas);

	gCtx.fillStyle = this.color;
	drawCircle(gCtx, 
			this.world.scale, 
			this.world.point2canvas, 
			this.shape.tc, 
			this.radius);
}

