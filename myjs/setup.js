//------------------------------------------------------------------------------------------
function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}
//------------------------------------------------------------------------------------------
var AppStart = setInterval(function() {
//	console.log(document.readyState);
    if (document.readyState === "complete") {
    	startApp();
        clearInterval(AppStart);
    }
}, 10);
//------------------------------------------------------------------------------------------
var restartApp = function()
{
	setTimeout(function () {
		if (app) 
		{
			app.myStop();
			app.stop();
		}
	}, 100);
}
//------------------------------------------------------------------------------------------
var startApp = function()
{
	console.log("FotoShot starting");

	image = getURLParameter('id');
	track = getURLParameter('track');
	
	if ( image && image.length > 8 )
		gUserImgSet = true;
	
	if ( track && track.length > 8 )
		gUserTrackSet = true;
	
	if (iOS || Android)
	{
		$("#fb-div").addClass("fb-div-ios");
		$("#fb-div").removeClass("div-hidden");
		
		$('#fb-btn').addClass("fb-btn-ios");

		$("#reset-div").addClass("reset-div-ios");
		$("#reset-div").removeClass("div-hidden");
		$('#reset-btn').addClass("reset-btn-ios");

		$("#app-store-div").addClass("app-store-div-ios");
		$("#app-store-div").removeClass("div-hidden");

		$("#app-store-img").addClass('app-store-img-ios');
		
		$("#app-store-download-free-img").addClass("app-store-download-free-img-ios");
		$("#app-store-download-free-img").removeClass("div-hidden");
	}
	else
	{
		$("#upload-header-div").addClass("upload-header-div-pc");
		$("#upload-header-div").removeClass("div-hidden");
		
		$("#upload-header-or-text").addClass("upload-header-or-text-div-pc");
		$("#upload-header-or-text").removeClass("div-hidden");

		$("#upload-header-or2-text").addClass("upload-header-or2-text-div-pc");
		$("#upload-header-or2-text").removeClass("div-hidden");

		$("#fb-div").addClass("fb-div-pc");
		$("#fb-div").removeClass("div-hidden");
		
		$("#reset-div").addClass("reset-div-pc");
		$("#reset-div").removeClass("div-hidden");
		
		$("#demo-div").addClass("demo-div-pc");
		$("#demo-div").removeClass("div-hidden");

		$("#app-store-div").addClass("app-store-div-pc");
		$("#app-store-div").removeClass("div-hidden");
		
		$("#upload-file-div").addClass("upload-file-div-pc");
		$("#upload-file-div").removeClass("div-hidden");
		
		$("#upload-url-div").addClass("upload-url-div-pc");
		$("#upload-url-div").removeClass("div-hidden");
	
		$("#app-store-download-free-img").addClass("app-store-download-free-img-pc");
		$("#app-store-download-free-img").removeClass("div-hidden");
		
//		$("#fb-friends-div").addClass("fb-friends-div-pc");
//		$("#fb-friends-div").removeClass("div-hidden");
		
		$("#get-friends-btn").addClass('get-friends-btn-pc');
		$('#get-friends-btn').removeClass('div-hidden');
		
		$('#fb-btn').addClass("fb-btn-pc");
		$('#reset-btn').addClass("reset-btn-pc");
		$('#demo-btn').addClass("demo-btn-pc");
		
		$('#upload-container-div').hide();
		
		$('#show-upload-btn').addClass('show-upload-btn-pc');
		$('#show-upload-btn').removeClass('div-hidden');
	}
	
	if ( Android )
	{
		$('#android-info-div').addClass('android-info-div');
		$('#android-info-div').removeClass('div-hidden');
		
		
	}
	gAppLoaded = true;
	app.run();
}