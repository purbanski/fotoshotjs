var puBall = function(world,radius,x,y)
{
	this.radius = radius;
	this.body = new cp.Body(1, Infinity);
	
	this.body.setPos(cp.v(x, y));
	
	this.shape = new cp.CircleShape(this.body, radius, cp.vzero);
	this.shape.setElasticity(0);
	this.shape.setFriction(0);
	
	this.world = world;
	
	this.color = 'rgb(0,255,0)';
	
	this.world.space.addBody(this.shape.getBody());
	this.world.space.addShape(this.shape);
//this.shape.setLayers(1);
}

puBall.prototype.setColor = function(r,g,b)
{
	var color;
	color = 'rgb('+ r + ',' + g + ',' + b +')';
	this.color = color;
}

puBall.prototype.render = function()
{
//	ball.shape.draw(gCtx, self.scale, self.point2canvas);

	gCtx.fillStyle = this.color;
	drawCircle(gCtx, 
			this.world.scale, 
			this.world.point2canvas, 
			this.shape.tc, 
			this.radius);
}

