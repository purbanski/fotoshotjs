//---------------------------------------------------------------
var trackEvent = function( category, action, label)
{
//	console.log(category);
	ga('send', 'event', category, action, label);
}
//---------------------------------------------------------------
var trackUser = function( action, label )
{
	trackEvent('User', action, label);
}
//---------------------------------------------------------------
var trackButton = function( label )
{
	trackEvent('User', 'Button-click', label);
}
//---------------------------------------------------------------
var trackSettings = function( action, label )
{
	trackEvent('Settings', action, label);
}
//---------------------------------------------------------------
var trackShare = function( action, label )
{
	trackEvent('Share', action, label);
}
//---------------------------------------------------------------
var trackUpload = function( label )
{
	trackEvent('User', 'Upload', label);
}
//---------------------------------------------------------------
var trackShot = function()
{
	trackEvent('User', 'Shooter', 'Shot');
}
//---------------------------------------------------------------
