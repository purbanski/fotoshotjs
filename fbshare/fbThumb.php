<?php

include_once '../php/imglib.php';
header('Content-Type: image/png');
	
$w = 100;
$h = 100;

$hashId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
if ( !isset($hashId))
{
	die("id not set");
}

$im = imagecreatetruecolor( $w, $h )
	or die("Cannot Initialize new GD image stream");

$background_color = imagecolorallocate($im, 55, 55, 56);

$radius = 2.2;
$dx = $radius;
$dy = $radius;


$json = fsGetJsonImgFromDb( $hashId );
// $json = fsGetJsonImgFromDb( '232186275aa002ec6f4f0b11c44e0f18' );

$data = json_decode($json, true);
$d = $data[0];
$balls = $d['balls'];

$cx = ($w - ( $d['width'] * $radius )) / 2 ;
$cy = ($h - ( $d['height'] * $radius )) / 2 ;

$ind = 0;

//-------------------
// Create background
$color = imagecolorallocate($im, 255,255,255);
$x1 = 0;
$y1 = 0;
$x2 = $w;
$y2 = $h;

// imagefilledrectangle ( $im , $x1 , $y1 , $x2, $y2, $color );

for ( $y = 0; $y < $d['height']; $y++ )
	for ( $x = 0; $x < $d['width']; $x++ )
	{
		$tx = $cx + $x * $dx;
		$ty = $cy + $y * $dy;
		
		$ind = $y * $d['width'] + $x;
		$ball = $balls[$ind];
		 		imagecolordeallocate ( $im, $color );
		
		$color = imagecolorallocate($im, $ball[0], $ball[1], $ball[2]);
		imagefilledellipse ( $im, $tx, $ty, $radius, $radius, $color );
// 		echo $ind." ". $ball[0]." ". $ball[1]." <br/>";
	
	}	
if ($im !== false) 
{
	
	imagepng($im);
// 	imagepng($im,"/tmp/th-".$hashId.".png");
    imagedestroy($im);
}
else {
    echo 'An error occurred.';
}
?>