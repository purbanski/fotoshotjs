<?php 
include_once '../php/urls.php';
?>
<!doctype html>
<html lang="en">

<head prefix="og: http://ogp.me/ns# product: http://ogp.me/ns/product#">
	<title>FotoShot</title>

	<meta property="fb:app_id" content="711333072242704" />
	<meta property="og:image" content="<?php echo fsGetFbThumbUrl(); ?>"/>
 	<meta property="og:type" content="website"/>

 	<meta property="og:title" content="FotoShot"/>
    <meta property="og:site_name" content="FotoShot - destroy any photos!" />
	<meta property="og:description" content='FotoShot - destroy any photos!' /> 
	<meta http-equiv="refresh" content="1; url=<?php echo fsGetRedirectUrl(); ?>" />
</head>
<body>
</body>
</html>