
<div id="fb-friends-div" class="div-hidden">
</div>

<script>
var FbFriends = [];

function fbShowWindow()
{
	var src = document.getElementById("fb-friends-div");

	var div = document.createElement("div");
	div.id = 'fb-friends-gen-div';
// 	div.class = 'fb-friends-div-pc';

	for (var i = 0; i<FbFriends.length; i++)
	{
//         console.log(i+" "+FbFriends[i].name);

		var url = FbFriends[i].picture.data.url;
		var ahref = document.createElement("a");
		var img = document.createElement("img");

		img.src = url;
		img.className = "fb-profile-pic";	
		img.onclick = function() {
			trackUpload("FB Friend");
			
			var url = '<?php require_once 'php/config.php'; echo Config::get('MainUrl'); ?>/php/upload/uploadFromWeb.php?url=' + this.src;
			window.location = url;
		}

   		div.appendChild(ahref);
		ahref.appendChild(img);
	}

	$("#fb-friends-div").addClass("fb-friends-div-pc");
	$("#fb-friends-div").removeClass("div-hidden");
	$("#fb-friends-div").hide();
	
	src.appendChild(div);
	$("#fb-friends-div").fadeIn();
}
//----------------------------------------------------------------------
function getFriends() {
	
	access_token = FB.getAuthResponse()['accessToken'];
	url = 'me/friends?fields=id,name,picture&access_token='+access_token;

	FB.api(url, function(response) {
        if(response.data) {
            FbFriends = response.data;
            fbShowWindow();
        }
	});
}
//----------------------------------------------------------------------
function fbLogin(){
	FB.login(function(response) {
		   if (response.authResponse) {
		     var access_token =   FB.getAuthResponse()['accessToken'];
// 		     console.log('Access Token = '+ access_token);
		     getFriends();
		   } 
		   else 
			{
// 		     console.log('User cancelled login or did not fully authorize.');
		   	}
		 }, {scope: ''});
}
//----------------------------------------------------------------------
$("#get-friends-btn").on('click', function() {
	fbLogin();
	trackButton("Pick a friend");
	// 	getFriends();
});
//----------------------------------------------------------------------
$("#show-upload-btn").on("click", function() {
	$('#show-upload-btn').fadeOut("slow");
	$("#upload-container-div").fadeIn();

	trackButton("Show upload container");
		
});

</script>