<div id="demo-div" class="div-hidden" >
	<button id="demo-btn" type="button" class="btn btn-info btn-lg">Demo: <b>ON</b></button>
</div>

<button id="show-upload-btn" type="button" class="div-hidden btn btn-warning btn-lg"><b>Upload new photos</b></button>


<div id="upload-container-div">

	<div id="upload-header-div" class="div-hidden">
		<b>Upload new photos</b>
	</div>

 
	<div id="upload-file-div" class="div-hidden">
		<button type="button" id="upload-file-btn" class="btn btn-warning btn-lg upload-file-btn" ><b>choose your file</b></button>
	</div>

	<div class="div-hidden">
		<form id="upload-file-form" action="php/upload/uploader.php" method="post" enctype="multipart/form-data">
			<input id="upload-file-field" type="file" name="file"  accept="image/jpeg,image/png" />
		</form>
	</div>

	<div id='upload-header-or-text' class='div-hidden'>
		<b>OR</b>
	</div>

	<div id='upload-header-or2-text' class='div-hidden'>
		<b>OR</b>
	</div>

	<div id="upload-url-div" class="div-hidden">
		<form id="upload-url-form" action="php/upload/uploadFromWeb.php" method="get" enctype="multipart/form-data">
			<input id="upload-url-field" placeholder="paste image url" class="form-control input-lg upload-url-field" type="text" name="url" />
		</form>
	</div>
	
	<button id="get-friends-btn" type="button" class="btn btn-primary btn-lg div-hidden"><b>pick a friend</b></button>
	
</div>
	
	
