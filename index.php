<!DOCTYPE HTML>
<html>
<head>
	<link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css"/>
	<link rel="stylesheet" href="libs/bootstrap/css/bootstrap-theme.css"/>
	<link rel="stylesheet" href="css/fotoshot.css"/>
	
	<link rel="apple-touch-icon" sizes="57x57" href="img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="img/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="img/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="img/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="img/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="img/favicons/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="img/favicons/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="img/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="img/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="img/favicons/mstile-144x144.png">
	<script src="libs/jquery.js"></script>
	
</head>

<body>
	<div id="fb-root"></div>
	<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '711333072242704',
          status     : true,
          xfbml      : true
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/all.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
	</script>
	
<?php
	include 'php/imglib.php';
	echo '<script>';
	echo fsGetJsonTrack();
	echo fsGetJsonImg();
	echo '</script>'
?>

<div id="fb-div" class="div-hidden" >
	<button id="fb-btn" class="btn btn-primary btn-lg">
		Share on <b>Facebook</b>
	</button>
</div>

<div id="app-store-div" class="div-hidden" >
	<a href="https://itunes.apple.com/app/id846152507" onclick="trackButton('AppStore');"><img id="app-store-img" src="img/download-ios.png"></a>
</div>

<a href="https://itunes.apple.com/app/id846152507" onclick="trackButton('AppStore');">
	<img id="app-store-download-free-img" class="div-hidden" src="img/download-free-label.png" />
</a>

<div id="reset-div" class="div-hidden" >
	<button id="reset-btn" type="button" class="btn btn-primary btn-lg"><b>Restart</b></button>
</div>
 
<?php 

	include_once 'php/agent.php';
	if ( iOS() )
	{
		include "browser/iOS/ios.php";
	}
	else if ( Android() )
	{
		include "browser/Android/android.php";
	}
	else
	{
		include "browser/PC/pc.php";
		include "browser/PC/facebook.php";
		
	}
?>
 
<!-- <div id="tabbar"></div> -->
<canvas id='can'></canvas>
<script src="js/cp.min.js"></script>

<script src="myjs/global.js"></script>
<script src="myjs/tracking.js"></script>
<script src="myjs/draw.js"></script>
<script src="myjs/world.js"></script>
<script src="myjs/shooter.js"></script>
<script src="myjs/ball.js"></script>
<script src="myjs/bullet.js"></script>
<script src="myjs/test-track.js"></script>
<script src="myjs/shotPlayer.js"></script>
<script src="myjs/setup.js"></script>
<script src="libs/bootstrap/js/bootstrap.js"></script> 

<script>

//------------------------------------------
// upload from pc
$("#upload-file-btn").on("click", function() {
	trackUpload("File");
	trackButton("Upload file");
	$('#upload-file-field').click();	
	
});

$('#upload-file-field').on("change", function() {
	var filename = $('#upload-file-field').val();
	if ( ! filename.length )
		return;

	$('#upload-file-form').submit();
});

//------------------------------------------
//upload from url
$('#upload-url-field').on("click", function() {
	$('#upload-url-field').val("");
});

$('#upload-url-field').on("paste", function() {
	var element = this;
	trackUpload("URL");
	trackButton("Upload url");
	
  	setTimeout(function () {
    	var url = $(element).val();
		$('#upload-url-form').submit();
		$(element).prop('disabled', true);
		$(element).addClass('upload-url-field-disable');
		$(element).removeClass('form-control');
		$(element).val("uploading...");
		
	}, 100);
});


</script>
 

 <script>
	var bar = document.getElementById('tabbar');
	
	app = new FotoShot
	
	var bnReset = document.getElementById('reset-btn');
	var bnClean = document.getElementById('demo-btn');
	var bnFacebook = document.getElementById('fb-btn');
	
// 	var cleanUp;

	var uploadUiReset = function() {
		$('#upload-container-div').fadeOut();
		$('#show-upload-btn').fadeIn();	
	};
	
	bnReset.onclick = function(){
		trackButton("Reset");

		uploadUiReset();
		restartApp();
 	};

	if ( bnClean )
	bnClean.onclick = function(){
		trackButton("Demo Start/Stop");
		uploadUiReset();
		
		if (gReplayEnable)
		{
			trackSettings("DemoBullets", "Disabled");

			bnClean.innerHTML = "Demo: <b>OFF</b>";
			gReplayEnable = false;
		}
		else
		{
			trackSettings("DemoBullets", "Enabled");

			bnClean.innerHTML = "Demo: <b>ON</b>";
			gReplayEnable = true;
		}
		restartApp();
	};

	bnFacebook.onclick = function(){
// 		if (iOS)
// 		{
//			var url = "<?php include_once 'php/urls.php'; echo fsGetFbShareUrl(); ?>";
// 			window.location.href = url;
// 		}
// 		else
		{
			
			trackButton("Facebook Share");
			
			FB.ui({
				method: 'feed',
				link: "<?php include_once 'php/urls.php'; echo fsGetRawFbShareUrl(); ?>", // 'https://fotoshot.blackted.com/',
				caption: 'Created with FotoShot',
				description: 'Destroy your photos any way you like!',
				}, function(response)
					{
						if (response && (response['post_id'].length) > 8 )
						{
							trackShare("Facebook","Shared");
						}
						else
						{
							trackShare("Facebook","Canceled");
						}

					});
		}
	};
</script>

 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php include_once 'php/config.php'; echo Config::get('GoogleTrackId'); ?>', 'blackted.com');
  ga('send', 'pageview');

</script>

 <script>
 	AppStart;
</script>

</body>
</html>