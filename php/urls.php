<?php 

include_once 'config.php';
//--------------------------------------------------------------
function fsGetRedirectUrl()
{
	$hashImgId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
	$hashTrackId = filter_input(INPUT_GET, 'track', FILTER_SANITIZE_SPECIAL_CHARS);
	
	if ( !isset($hashImgId))
		$hashImgId="";
	
	if ( !isset($hashTrackId))
		$hashTrackId="";
		
	$url = Config::get('MainUrl');
	$url .= '/?id=';
	$url .= $hashImgId . "&track=" . $hashTrackId; 
	
	return $url;
}
//--------------------------------------------------------------
function fsGetRawFbShareUrl()
{
	$hashImgId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
	$hashTrackId = filter_input(INPUT_GET, 'track', FILTER_SANITIZE_SPECIAL_CHARS);
	
	if ( !isset($hashImgId))
		$hashImgId="";
	
	if ( !isset($hashTrackId))
		$hashTrackId="";
	
// 	$url .= Config::get('MainUrl');
	$mainUrl = "http://fotoshot.blackted.com";
	$mainUrl .= '/fbshare/share2.php?';
	$args = 'id=' . $hashImgId . "&track=" . $hashTrackId; 
	
	return $mainUrl . $args;
}
//-//--------------------------------------------------------------
function fsGetFbShareUrl()
{
	$hashImgId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
	$hashTrackId = filter_input(INPUT_GET, 'track', FILTER_SANITIZE_SPECIAL_CHARS);
	
	if ( !isset($hashImgId))
		$hashImgId="";
	
	if ( !isset($hashTrackId))
		$hashTrackId="";
	
	$url ='';
	$FBurl = 'http://www.facebook.com/share.php?u=';
// 	$url .= Config::get('MainUrl');
	$url .= "http://fotoshot.blackted.com";
	$url .= '/fbshare/share2.php?id=';
	$url = $url . $hashImgId . "&track=" . $hashTrackId; 
	
	return $FBurl . urlencode($url);
}
//--------------------------------------------------------------
function fsGetFbThumbUrl()
{
	$hashId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
	
	if ( !isset($hashId) || strlen($hashId) < 16)
	{
		$url = Config::get('MainUrl');
		$url .= '/img/logo-100.png';
		return $url;
	}

	$url = Config::get('MainUrl');
	$url .= '/fbshare/fbThumb.php?id=';
	$url .= $hashId;
	
	return $url;
}
//--------------------------------------------------------------

?>