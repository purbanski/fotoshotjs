<?php 
//-----------------------------------------------------
include_once 'imglib.php';
include_once 'config.php';
//-----------------------------------------------------
function fsImgScale( $source, $maxWidth, $maxHeight )
{
	$origWidth = imagesx( $source );
	$origHeight = imagesy( $source );
	
	$scalew = $maxWidth / $origWidth;
	$scaleh = $maxHeight / $origHeight;
	$scale = min( $scalew, $scaleh );
	
	$newWidth = $origWidth * $scale;
	$newHeight = $origHeight * $scale;

	if ( Config::get('DEBUG') )
	{
		echo "ow: ". $origWidth . " oh:". $origHeight."<br/>";
		echo "mw: ". $maxWidth . " mh:". $maxHeight."<br/>";
		echo "w: " . $scalew . " h:".$scaleh . "<br/>";
		echo "nw: ". $newWidth . " nh:". $newHeight."<br/>";
// 		die();	
	}
	
	$newImage = imagecreatetruecolor($newWidth, $newHeight );

	// preserve the alpha for pngs
	imagealphablending($newImage, false);
	imagesavealpha($newImage, true);
	
	$ret = imagecopyresampled($newImage, $source, 0, 0, 0, 0, $newWidth , $newHeight , $origWidth, $origHeight);
	if ( ! $ret )
		die();
	
	return $newImage;
}
//-----------------------------------------------------
function getJsonFromGDImg( $im )
{
	$im = fsImgScale($im,
			Config::get('ImgConvMaxWidth'),
			Config::get('ImgConvMaxHeight'));
	
	$img_width = imagesx( $im );
	$img_height = imagesy( $im );

	$toJson = array();
	$points = array();
	
	for ( $y = 0; $y < $img_height; $y++ )
	for ( $x = 0; $x < $img_width; $x++ )
	{
		$rgb = imagecolorat($im, $x, $y);
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
		array_push( $points, array( $r,$g,$b ));
	}
	
	array_push($toJson, array( 'ver' => Config::get('JsonVer'),
								'width'=>$img_width, 
								'height'=>$img_height, 
								'bubbleScale'=> Config::get('BubbleScale'), 
								'balls'=>$points  ));
							
	return json_encode($toJson);
}
//-----------------------------------------------------
function fsGetJsonImgFromUrl( $img_url )
{
	// 	list($img_width, $img_height, $type, $attr) = getimagesize($img_url);
	$headers = @get_headers($img_url);
	$exists = preg_match('/HTTP\/[0-9].[0-9] 200 OK/',$headers[0] );

	if ( ! $exists )
	{
		return fsGetJsonImgRandom();
// 		die('File doesn\'t exists');
	}

	if (preg_match('/\.png$/i',$img_url))
	{
		$im = imagecreatefrompng($img_url);
	}
	else if ( 
		preg_match('/.\jpg$/i',$img_url) ||
		preg_match('/.\jpeg$/i',$img_url) )
	{
		$im = imagecreatefromjpeg($img_url);
	}
	else if (preg_match('/\.gif$/i',$img_url))
	{
		$im = imagecreatefromgif($img_url);
	}
	else
	{
		// i could download remote file
		// and try to create from string
		// it might work
		
// 		$im = imagecreatefromstring($img_url);
		return fsGetJsonImgRandom();
		die( $im .  " Wrong format" );
	}
	
	if ( ! $im )
	{
		return fsGetJsonImgRandom();
		die("null img");
	}

	return getJsonFromGDImg($im);
}
//-----------------------------------------------------
function fsGetJsonImgFromString( $binData )
{
	$im = imagecreatefromstring( $binData );
	return getJsonFromGDImg( $im );
}
//-----------------------------------------------------
function fsGetJsonImgFromFile( $filename )
{
	$img_url = "php/" . $filename;
	return fsGetJsonImgFromUrl( $img_url );
}
//-----------------------------------------------------
function fsGetJsonImgFromDb( $hashId )
{
	$sql = "
		SELECT * 
		FROM ". Config::get('DBName') .".Image 
		WHERE hash = '". $hashId ."';";
		
	//---
	$con = mysql_connect(
			Config::get('DBServer'),
			Config::get('DBUsername'),
			Config::get('DBPassword'));
	
	if (!$con)
	{
		die('Could not connect: ' . mysql_error());
	}
	// mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
	//---
	
	$result = mysql_query($sql,$con);
	if ( ! $result )
	{
		die('Problem - most likely duplicated');
	}
	
	$row = mysql_fetch_assoc($result);
	mysql_close($con);

	return $row['json'];
}
//-----------------------------------------------------
function fsGetJsonTrackFromDb( $hashId )
{
	$sql = "
		SELECT *
		FROM ". Config::get('DBName') .".Track
		WHERE hash = '". $hashId ."';";

	//---
	$con = mysql_connect(
			Config::get('DBServer'),
			Config::get('DBUsername'),
			Config::get('DBPassword'));

	if (!$con)
	{
		die('Could not connect: ' . mysql_error());
	}
	// mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
	//---

	$result = mysql_query($sql,$con);
	if ( ! $result )
	{
		die('Problem - most likely duplicated');
	}

	$row = mysql_fetch_assoc($result);
	mysql_close($con);

	return $row['json'];
}
//-----------------------------------------------------
function fsGetJsonTrackRandom()
{
	$sql = "
		SELECT *
		FROM ". Config::get('DBName') .".Track
		ORDER BY RAND()
		LIMIT 1;";

	//---
	$con = mysql_connect(
			Config::get('DBServer'),
			Config::get('DBUsername'),
			Config::get('DBPassword'));

	if (!$con)
	{
		die('Could not connect: ' . mysql_error());
	}
	// mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
	//---

	$result = mysql_query($sql,$con);
	$row = mysql_fetch_assoc($result);
	mysql_close($con);

	return $row['json'];
}
//-----------------------------------------------------
function fsGetJsonImgRandom()
{
	$sql = "
		SELECT *
		FROM ". Config::get('DBName') .".Image
		ORDER BY RAND()
		LIMIT 1;";

	//---
	$con = mysql_connect(
			Config::get('DBServer'),
			Config::get('DBUsername'),
			Config::get('DBPassword'));

	if (!$con)
	{
		die('Could not connect: ' . mysql_error());
	}
	// mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $con);
	//---

	$result = mysql_query($sql,$con);
	$row = mysql_fetch_assoc($result);
	mysql_close($con);

	return $row['json'];
}
//-----------------------------------------------------
function fsGetJsonImg()
{
	$filename = filter_input(INPUT_GET, 'img', FILTER_SANITIZE_SPECIAL_CHARS);
	$url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_SPECIAL_CHARS);
	$hashId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
	
	if ( isset( $url ))
	{
		$imgData = fsGetJsonImgFromUrl($url);
	}
	else if ( isset( $filename ))
	{
		$imgData = fsGetJsonImgFromFile($filename);
	}	
	else if ( isset( $hashId ) && strlen( $hashId )>16 )
	{
		$imgData = fsGetJsonImgFromDb( $hashId );
	}
	else 
	{
		$imgData = fsGetJsonImgRandom();
	}
	
	echo "fsJsonImg = " . $imgData . ";\n";
}
//-----------------------------------------------------
function fsGetJsonTrack()
{
	$hashId = filter_input(INPUT_GET, 'track', FILTER_SANITIZE_SPECIAL_CHARS);

	if ( isset( $hashId ) && strlen( $hashId )>16 )
	{
		$trackData = fsGetJsonTrackFromDb( $hashId );
	}
	else
	{
		$trackData = fsGetJsonTrackRandom();
	}

	echo "\nfsJsonShotTrack = " . $trackData . ";\n\n";
}

//-----------------------------------------------------
function fsGetPngThumbnail( $hashId )
{
	$json = fsGetJsonImgFromDb( $hashId );
	var_dump($json);
	die();
}
?>