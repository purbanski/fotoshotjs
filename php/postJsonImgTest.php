<?php 
//----------------------------------------------------------------------
include_once 'config.php';
include_once 'postJsonImgLib.php';
include_once 'imglib.php';
//----------------------------------------------------------------------
function testPostJsonImg()
{
	$filename = filter_input(INPUT_GET, 'img', FILTER_SANITIZE_SPECIAL_CHARS);
	$url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_SPECIAL_CHARS);

	if ( isset( $url ))
	{
		$imgData = fsGetJsonImgFromUrl($url);
	}
	else if ( isset( $filename ))
	{
		$imgData = fsGetJsonImgFromFile($filename);
	}
	else
	{
		$imgData = "";
		die("Not image specified");
	}
	
	return $imgData;
}
//----------------------------------------------------------------------
$mydata = testPostJsonImg();
fsPostJsonImg( $mydata );
//----------------------------------------------------------------------
?>