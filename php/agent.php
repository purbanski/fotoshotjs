<?php 

function iOS()
{
	$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
	$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
	
	if ( $iPod || $iPhone || $iPad )
		return true;
	return false;
}

function Android()
{
	$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
	if ( $Android )
		return true;
	return false;
}

?>