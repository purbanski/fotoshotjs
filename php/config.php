<?php 
//----------------------------------------------------------------
date_default_timezone_set('Europe/Berlin');

$cfg['JsonVer'] 	= "1.0";
$cfg['BubbleScale']	= 1;

$cfg['Timestamp'] 		= date('[Y:m:d][H:i:s]') ."\n";
$cfg['FrontendUrl'] 	= 'http://trup.blackted.com';
$cfg['ClientIP']		= $_SERVER['REMOTE_ADDR'];

$cfg['ImgConvMaxWidth'] 	= 40;
$cfg['ImgConvMaxHeight'] 	= 40;
$cfg['UploadDir']			= '/tmp';
$cfg['DEBUG']				= false;

$cfg['MainUrl']			= 'http://fotoshot.blackted.com';
$cfg['DBServer']		= 'localhost';
$cfg['DBUsername']		= 'fs';
$cfg['DBPassword']		= '$h4sl0NIETr7wi';
$cfg['DBName']			= 'FotoShot';
$cfg['GoogleTrackId']	= 'UA-38130172-12';		// FotoShot Web 

$cfg['MainUrl']			= 'http://fsdev.blackted.com';
$cfg['DBServer']		= 'localhost';
$cfg['DBUsername']		= 'fs';
$cfg['DBPassword']		= '12345';
$cfg['DBName']			= 'FotoShot';
$cfg['GoogleTrackId']	= 'UA-38130172-13';   // FotoShot Web Dev

//----------------------------------------------------------------
Class Config
{
	static protected $config = array();
	private function __construct() {} //make this private so we can't instanciate
	public static function set($key, $val)
	{
		Config::$config[ $key ] = $val;
	}
	//-----------------------------------------------------
	public static function get($key)
	{
		if ( !isset(Config::$config[ $key ]))
			return "";
		
		return Config::$config[ $key ];
	}
	//-----------------------------------------------------
	public static function setup()
	{
		global $cfg;
		foreach( $cfg as $key => $value )
		{
			Config::set($key, $value);
		}
	}
	//-----------------------------------------------------
}
//----------------------------------------------------------------
Config::setup();

?>
