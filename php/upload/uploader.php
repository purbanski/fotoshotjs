<?php
include_once '../config.php';
include_once '../postJsonImgLib.php';
include_once '../imglib.php';

// var_dump($_FILES);
$type = $_FILES['file']['type'];

if ( $type != 'image/png' && $type != 'image/jpeg' )
{
// 	die("redirect");
	header("Location:".Config::get("MainUrl"));
}

if( $_FILES['file']['name'] != "" )
{
	$binImg = file_get_contents($_FILES['file']['tmp_name']);
	$jsonImg =  fsGetJsonImgFromString( $binImg);
	fsPostJsonImg( $jsonImg );
	$hashId = md5($jsonImg);

	$destUrl = Config::get('MainUrl').'/?id='.$hashId;
	header("Location: ".$destUrl);
}
?>
