<?php 
include_once '../config.php';
include_once '../imglib.php';
include_once '../postJsonImgLib.php';

$url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);

if ( !isset($url) || strlen($url) < 8 )
{
	die("");
}

$jsonImg = fsGetJsonImgFromUrl($url);
fsPostJsonImg( $jsonImg );
$hashId = md5($jsonImg);

$destUrl = Config::get('MainUrl').'/?id='.$hashId;
header("Location: ".$destUrl);

?>