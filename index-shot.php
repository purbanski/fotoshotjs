<!DOCTYPE HTML>
<html>
<head>
	<link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css"/>
	<link rel="stylesheet" href="libs/bootstrap/css/bootstrap-theme.css"/>
	<link rel="stylesheet" href="css/fotoshot.css"/>
</head>

<body>
	<div id="fb-root"></div>
	<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '711333072242704',
          status     : true,
          xfbml      : true
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/all.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
	</script>
    
<?php
include 'php/imglib.php';
echo fsGetJsonImg();
?>

<!-- <button id="b1"> Restart</button> -->
<a class="btn btn-primary btn-sm"
	style="margin-right: 2px;"
	href='?id=9ea7a101594bc150e7cb7fcf0fecad7f'>
	<b>Urbas</b>
</a>
<a class="btn btn-primary btn-sm"
	style="margin-right: 2px;"
	href='?id=232186275aa002ec6f4f0b11c44e0f18'>
	<b>Antoni</b>
</a>
<a style="margin-right: 2px;"
	<?php 
	include_once 'php/urls.php';
	echo fsGetFbShareUrl();
	?>
	<b>Facebook</b>
</a>


<div id="tabbar"></div>
<canvas style="background-color: #121212" width="960" height="640"id='can'></canvas>
<script src="js/cp.min.js"></script>

<!-- 
<script src="js/demo.js"></script>
<script src="js/LogoSmash.js"></script>
<script src="js/ball.js"></script>
<script src="js/Joints.js"></script>
<script src="js/PyramidStack.js"></script>
<script src="js/PyramidTopple.js"></script>
<script src="js/Convex.js"></script>
<script src="js/Query.js"></script>
<script src="js/buoyancy.js"></script>
-->
<script src="libs/jquery.js"></script>
<script src="myjs/global.js"></script>
<script src="myjs/draw.js"></script>
<script src="myjs/world.js"></script>
<script src="myjs/shooter.js"></script>
<script src="myjs/ball.js"></script>
<script src="myjs/bullet.js"></script>
<script src="myjs/test.js"></script>

<script src="libs/bootstrap/js/bootstrap.js"></script> 

<script>
	var bar = document.getElementById('tabbar');
	var current = null;

	current = new demos[0].demo
	current.run();

	$('#b1').on('click',function(){
		
		if (current) 
		{
			console.log("curent stop");
			current.stop();
			delete current;
		}
		current = new demos[0].demo
		current.run();

	});
</script>



</body>
</html>